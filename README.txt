mrtigra vanilla_show module

This module is about the simplest way to get a mrtigra vanilla_show, or a link to a
mrtigra vanilla_show, displayed with your content. 

INSTALLTION:
- Upload/unzip to your Drupal 7 sites/all/modules directory (or
  sites/default/modules or whatever).
- Enable the module.


USE:
In order to place the application on a page of the site, you need to select any pre-existing page or create a new one, and then when editing the page's content to place the macro has 
  the following syntax:
  [mrtigra.vanilla_show [rid]] Where rid is the identifier of parthner.

  Examples:
  With specifying parthner indeifier (rid) 1ada801075c101918a1e9fa44cd4f373:
  [mrtigra.vanilla_show 1ada801075c101918a1e9fa44cd4f373]
  
  Without specifying parthner indeifier:
  [mrtigra.vanilla_show]
  
  You can specify the affiliate ID (rid) in the module settings
  [mrtigra.vanilla_show 1ada801075c101918a1e9fa44cd4f373]

  Usage by structure block:
  
    In the section Structure/Blocks you need find and select «Реалити шоу» module
    In the dropdown list of the usage module you need select target area and save the settings.
    To make revenue you need specify you RID in the settings section of module.
  

  Affiliate program
  To placing components of application of «Master tigra team» on webpages you will become an participant of <a href=http://mastertigra.com/'>  affiliate program</a> and can earn income. <br>To make revenue you need specify your RID in the settings section of module.<br>You can get the identifier on the http://mastertigra.com/profile personal profile webpage on website of «Master tigra» team: http://mastertigra.com

  Technical support
  At any time specialists of «Master Tigra» team can provide you technical assistance and answering on your questions about the affiliate program and about how to use the components: http://mastertigra.com/en/support#app=live2
