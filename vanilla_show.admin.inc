<?php

/**
 * mrtigra config form.
 */
function vanilla_show_config_form($form, &$form_state) {
  $form = array();
 
  $form['vanilla_show_rid'] = array(
    '#type' => 'textfield',
    '#title' => t(vanilla_show_lang("rid_header")),
    '#default_value' => variable_get('vanilla_show_rid', ""),
    '#description' => t(vanilla_show_lang("rid_descr")),
    '#required' => FALSE,
  );
  
  $form['vanilla_show_description'] = array(
    '#type' => 'item',
    '#title' => vanilla_show_lang("guide_header"),
    '#markup' => vanilla_show_lang("guide"),
  );
  return system_settings_form($form);
}