<?php
  
  $mrtigra_vanillashow_dictionary = array(
    "title" => "Реалити шоу",
    "descr" => "Видеочат «Реалити-шоу» для размещения на страницы сайта",
    "rid_header" => "Основной партнерский идентификатор (RID)",
    "rid_descr" => "Необходим для участия в партнерской программе. Это значение используется по умолчанию для всех размещаемых приложений «Гномоград», если не задан явно другой RID.",
    "guide_header" =>  "<h3>Руководство по размещению и настройке.</h3>",
    "guide" => 
      "<div class=INFO_VENDORIZE>
        <h4>Подключение с использованием shortcode в текстовом редакторе контента (WYSIWYG)</h4>
        <ol>
          <li>Создать или выбрать существующую страницу.</li>
          <li>
            <span>Используя редактор контента страницы (WYSIWYG) укажите специальный shortcode, следующего вида:</span>
            <span><code>[mrtigra.vanillashow &#123;RID&#125;]</code> или <code>[mrtigra.vanillashow rid=\"&#123;RID&#125;\"]</code>
            <br>Где &#123;RID&#125; это партнерский идентификатор.</span>
          </li>
        </ol>
        
        
        <h4>Примеры:</h4>
        <ol>
          <li>
            <span>С указанием идентификатора партнёра &#123;RID&#125; 1ada801075c101918a1e9fa44cd4f373:</span>
            <span><code>[mrtigra.vanillashow 1ada801075c101918a1e9fa44cd4f373]</code></span>
            <span>или</span>
            <span><code>[mrtigra.vanillashow rid=\"1ada801075c101918a1e9fa44cd4f373\"]</code></span>
            <span>Таким образом возможно размещение приложения с разными партнёрскими идентификаторами &#123;RID&#125;.</span>
          </li>
          <li>
            <span>Без указания идентификатора партнёра (идентификатор будет взят из настроек модуля):</span>
            <span><code>[mrtigra.vanillashow]</code></span>
          </li>
        
        <h4>Подключение с помощью блоков</h4>
        <ol>
          <li>В разделе Администрирование ⇒ Структура ⇒ Блоки панели администрирования необходимо найти и выбрать блок «Реалити шоу».</li>
          <li>В выпадающем списке выбрать регион для подключения блока и сохранить настройки.</li>
          <li>Для участия в партнерской программе необходимо указать ваш партнёрский идентификатор &#123;RID&#125; в разделе настроек модуля.</li>
        </ol>
        
        <h4>Партнерская программа</h4>
        <p>Размещая на страницах сайта компоненты приложений команды «Мастера Тигры» вы становитесь участником <a href='//mastertigra.com/'>партнёрской программы</a> и можете получать доход.</p>
        <p>В настройках модуля или коде размещения необходимо прописать ваш партнёрский идентификатор &#123;RID&#125;. Идентификатор партнёра можно получить <a href='//mastertigra.com/profile'>в личном кабинете партнёра</a> на сайте команды «Мастера Тигры» : <a href='//mastertigra.com/'>http://mastertigra.com/</a>.</p>
        
        <h4>Техническая поддержка</h4>
        <p>Специалисты команды «Мастера Тигры» окажут вам полноценную техническую поддержку и ответят на все ваши вопросы по партнёрской программе и размещению приложений: <a href='//mastertigra.com/support#app=live2'>http://mastertigra.com/support#app=live2</a>.</p>
        </div>

      <style>
      .INFO_VENDORIZE { text-align: left; padding-left: 5px; }
      .INFO_VENDORIZE a {text-decoration: underline; }
      .INFO_VENDORIZE li  { margin-top: 10px; }
      .INFO_VENDORIZE li span { display: block; margin-top: 5px; }
      .INFO_VENDORIZE code { margin-top: 12px; } 
      .INFO_VENDORIZE h3,h4 { margin-top: 30px; } 
      .INFO_VENDORIZE p { padding-top: 5px; margin-top: 10px; line-height: 27px;}
      .INFO_VENDORIZE code { border: 1px solid rgb(220, 220, 220); font-size: 100%; padding: 5px; border-radius: 4px; box-shadow: 0px 5px 5px -5px rgba(0, 0, 0, 0.15);}
      </style>
      ",
  );
?>