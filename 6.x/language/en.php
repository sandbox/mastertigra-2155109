<?php
  
  $mrtigra_vanillashow_dictionary = array(
    "title" => "Vanilla Show",
    "descr" => "Video chat «Vanilla Show» to be placed on the site pages.",
    "rid_header" => "Main parthner identifier (RID)",
    "rid_descr" => "Required for participation in the affiliate program. This value RID is used as the default for all placed applications «Vanilla Show», if not set other RID.",
    "guide_header" =>  "<h3>User guide</h3>",
    "guide" => 
      "<div class=INFO_VENDORIZE>
        <h4>Placement via using shortcode in a content text editor (WYSIWYG)</h4>
        <ol>
          <li>Select any pre­existing page or create a new one.</li>
          <li>
            <span>Using the content editor of page (WYSIWYG) specify special shortcode:</span>
            <span><code>[mrtigra.vanillashow &#123;RID&#125;]</code> or <code>[mrtigra.vanillashow rid=\"&#123;RID&#125;\"]</code>
            <br>Where &#123;RID&#125; is the identifier of parthner.</span>
          </li>
        </ol>
        
        <h4>Examples:</h4>
        <ol>
          <li>
            <span>With specifying affiliate identifier &#123;RID&#125; 1ada801075c101918a1e9fa44cd4f373:</span>
            <span><code>[mrtigra.vanillashow rid=\"1ada801075c101918a1e9fa44cd4f373\"]</code></span>
            <span>or</span>
            <span><code>[mrtigra.vanillashow 1ada801075c101918a1e9fa44cd4f373]</code></span>
            <span>Thus you can have a multiple applications with different affiliate identifiers &#123;RID&#125;.</span>
          </li>
          <li>
            Without specifying parthner indeifier (RID will be taken from the module settings):
            <br>
            <code>[mrtigra.vanillashow]</code>
          </li>
        </ol>

        <h4>Usage by structure block</h4>
        <ol>
          <li>Find and select «Vanilla Show» block in the Administer ⇒ Site building ⇒ Blocks section of administration panel.</li>
          <li>Select region In the dropdown list of module and save the settings.</li>
          <li>To make revenue you need specify you affiliate identifier &#123;RID&#125; in the settings section of module.</li>
        </ol>
        
        <h4>Affiliate program</h4>
        <p>To placing components of application of «Master Tigra team» on webpages you will become an participant of <a href='//mastertigra.com/en'>affiliate program</a> and can earn income.</p>
        <p>To make revenue you need specify your affiliate identifier &#123;RID&#125; in the settings section of module. You can get the identifier on the <a href=//mastertigra.com/en/profile/>personal profile</a> webpage on website of «Master Tigra» team: <a href='//mastertigra.com/en/'>http://mastertigra.com/en/</a>.</p>
        
        <h4>Technical support</h4>
        <p>At any time specialists of «Master Tigra» team can provide you technical assistance and answering on your questions about the affiliate program and how to use the components: <a href='//mastertigra.com/en/support#app=live2'>http://mastertigra.com/en/support#app=live2</a>.</p>
      </div>

      <style>
      .INFO_VENDORIZE { text-align: left; padding-left: 5px; }
      .INFO_VENDORIZE a {text-decoration: underline; }
      .INFO_VENDORIZE li  { margin-top: 10px; }
      .INFO_VENDORIZE li span { display: block; margin-top: 5px; }
      .INFO_VENDORIZE code { margin-top: 12px; } 
      .INFO_VENDORIZE h3,h4 { margin-top: 30px; } 
      .INFO_VENDORIZE p { padding-top: 5px; margin-top: 10px; line-height: 27px;}
      .INFO_VENDORIZE code { border: 1px solid rgb(220, 220, 220); font-size: 100%; padding: 5px; border-radius: 4px; box-shadow: 0px 5px 5px -5px rgba(0, 0, 0, 0.15);}
      </style>
    ",
  );
  
?>