<?php

/**
 * mrtigra config form.
 */
function mrtigra_vanillashow_config_form($form, &$form_state) {
  $form = array();
 
  $form['mrtigra_vanillashow_rid'] = array(
    '#type' => 'textfield',
    '#title' => t(mrtigra_vanillashow_lang("rid_header")),
    '#default_value' => variable_get('mrtigra_vanillashow_rid', ""),
    '#description' => t(mrtigra_vanillashow_lang("rid_descr")),
    '#required' => FALSE,
  );
  
  $form['mrtigra_vanillashow_description'] = array(
    '#type' => 'item',
    '#title' => mrtigra_vanillashow_lang("guide_header"),
    '#markup' => mrtigra_vanillashow_lang("guide"),
  );
  return system_settings_form($form);
}